

import java.util.LinkedList;
import java.util.List;

public class ScreenRecorder {

	private List<Profile > listOfProfiles = new LinkedList<>();
	private Profile currentProfile = null;
	private Boolean isRecording = false;
	
	public void startRecording(){
		if(currentProfile == null){
			System.out.println("Set Profile");
		}else if(isRecording){
			System.out.println("Currently recording");
		}else{
			System.out.println("Recording started: " + currentProfile);
		}
	}
	
	public void stopRecording(){
		if(isRecording){
			System.out.println("Recording stopped");
		}else{
			System.out.println("Nothing to stop.");
		}
	}
	
	public void addProfile(Profile p ){
		listOfProfiles.add(p);
	}
	
	public void listProfiles(){
		for(int i=0; i<listOfProfiles.size(); i++){
			System.out.println((i+1)  + " : " + listOfProfiles.get(i));
		}
	}
	
	public void setProfile(int index){
		if(index <1 || index > listOfProfiles.size()){
			System.out.println("Podany profil nie istnieje");
		}else{
			currentProfile = listOfProfiles.get(index);
		}
	}
}
